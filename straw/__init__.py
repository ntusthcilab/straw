import random

from flask import (
    Blueprint,
    current_app,
    redirect,
)


bp = Blueprint('straw', __name__)


@bp.before_app_first_request
def setup_random_seed():
    random.seed(current_app.config.get('SECRET_KEY'))


@bp.route('/<key>')
def entrance(key):
    terminals = current_app.config.get('STRAWS', {})
    options = terminals.get(key)

    if options is not None:
        destination = random.choice(options)
        return redirect(destination)
    else:
        return f'Invalid key: {key}', 400
