import os

from flask import Flask

from straw import bp as straw


def create_app(config=None):
    app = Flask(__name__)
    app.config.from_mapping(
        SECRET_KEY='dev',
        STRAWS={},
    )

    if config is not None:
        app.config.from_object(config)

    app.register_blueprint(straw, url_prefix='/')

    return app


if __name__ == '__main__':
    app = create_app('config.Config')
    app.run()
